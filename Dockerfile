# iprouteget frr docker!

FROM frrouting/frr-debian:latest

RUN apt-get update && apt-get upgrade -yqq
RUN apt-get install -yqq --no-install-recommends \
    tree \
    openssh-server \
    sudo

RUN mkdir /home/frr && chown -R frr:frr /home/frr

RUN usermod -d /home/frr -s /bin/bash -p $(echo "frr" | openssl passwd -1 -stdin) frr

RUN usermod -a -G sudo frr

RUN usermod -u 1000 frr

COPY frr/* /etc/frr/

RUN rm /etc/frr/frr.conf

RUN chown -R frr:frr /etc/frr

COPY ./entrypoint.sh /

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
