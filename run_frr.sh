# run_frr.sh
#
# Run the FRR docker container
set -e

NAME=$1

if [ "$1" == "" ]; then
    echo "Please provide a name for the container"
    exit
fi

docker run -d \
	   -it \
	   --name ${NAME} \
	   --network mgmt \
	   --hostname ${NAME} \
	   --privileged \
	   frr
