#!/usr/bin/env bash

set -e

echo "Starting ssh and frr service..."

sudo /etc/init.d/ssh start
sudo /etc/init.d/frr restart

echo "Done"

sleep 10000d
